function Monitor (onUpdate) {

    function process (loadavgHistorySource, memoryHistorySource,
        networkHistorySource, storageHistorySource, temperatureHistorySource,
        loadavgHistoryDestination, memoryHistoryDestination,
        networkHistoryDestination, storageHistoryDestination,
        temperatureHistoryDestination, destinationLimit, sourceLimit) {

        function forEach (values, callback) {
            var numItems = 0
            for (var i = 0; i < sourceLimit; i++) {
                if (i == values.length) break
                callback(values[i])
                numItems++
            }
            return numItems
        }

        function push (values, value) {
            pushLimit(values, value, destinationLimit)
        }

        var time = Date.now()

        var value0 = 0, value1 = 0, value2 = 0
        var numItems = forEach(loadavgHistorySource, function (historyValue) {
            value0 += historyValue.value[0]
            value1 += historyValue.value[1]
            value2 += historyValue.value[2]
        })
        push(loadavgHistoryDestination, {
            time: time,
            value: [
                value0 / numItems,
                value1 / numItems,
                value2 / numItems,
            ],
        })

        var value = 0
        var numItems = forEach(memoryHistorySource, function (historyValue) {
            value += historyValue.value
        })
        push(memoryHistoryDestination, {
            time: time,
            value: value / numItems,
        })

        var sent = 0, received = 0
        var numItems = forEach(networkHistorySource, function (historyValue) {
            sent += historyValue.sent
            received += historyValue.received
        })
        push(networkHistoryDestination, {
            time: time,
            sent: sent / numItems,
            received: received / numItems,
        })

        var total = 0, used = 0, fileSystems = []
        var numItems = forEach(storageHistorySource, function (historyValue) {
            total += historyValue.total
            used += historyValue.used
            historyValue.fileSystems.forEach(function (fileSystem) {

                var total = fileSystem.total,
                    used = fileSystem.used,
                    mountPoint = fileSystem.mountPoint

                var found = false
                fileSystems.forEach(function (fileSystem) {
                    if (fileSystem.mountPoint == mountPoint) {
                        fileSystem.total += total
                        fileSystem.used += used
                        fileSystem.numItems++
                        found = true
                        return false
                    }
                })
                if (!found) {
                    fileSystems.push({
                        total: total,
                        used: used,
                        mountPoint: mountPoint,
                        numItems: 1,
                    })
                }

            })
        })
        fileSystems.forEach(function (fileSystem) {
            var numItems = fileSystem.numItems
            fileSystem.total /= numItems
            fileSystem.used /= numItems
            delete fileSystem.numItems
        })
        push(storageHistoryDestination, {
            time: time,
            total: total / numItems,
            used: used / numItems,
            fileSystems: fileSystems,
        })

        var cpu = 0, mb = 0
        var cpuNull = false, mbNull = false
        var numItems = forEach(temperatureHistorySource, function (historyValue) {

            var historyCpu = historyValue.cpu
            if (historyCpu === null) cpuNull = true

            var historyMb = historyValue.mb
            if (historyMb === null) mbNull = true

            cpu += historyCpu
            mb += historyMb

        })

        if (cpuNull) cpu = null
        else cpu = cpu / numItems

        if (mbNull) mb = null
        else mb = mb / numItems

        push(temperatureHistoryDestination, {
            time: time,
            cpu: cpu,
            mb: mb,
        })

    }

    function pushLimit (values, value, limit) {
        values.push(value)
        if (values.length > limit) values.shift()
    }

    var loadavgHistory = [],
        loadavgHistory20 = [],
        loadavgHistory120 = [],
        loadavgHistory600 = [],
        loadavgHistory7200 = [],
        memoryHistory = [],
        memoryHistory20 = [],
        memoryHistory120 = [],
        memoryHistory600 = [],
        memoryHistory7200 = [],
        networkHistory = [],
        networkHistory20 = [],
        networkHistory120 = [],
        networkHistory600 = [],
        networkHistory7200 = [],
        storageHistory = [],
        storageHistory20 = [],
        storageHistory120 = [],
        storageHistory600 = [],
        storageHistory7200 = [],
        temperatureHistory = [],
        temperatureHistory20 = [],
        temperatureHistory120 = [],
        temperatureHistory600 = [],
        temperatureHistory7200 = []

    var programLastTime = Date.now(),
        uptimeTotal = Math.floor(os.uptime() * 1000),
        downtimeTotal = 0

    var storage = {
        total: 0,
        used: 0,
        fileSystems: [],
    }

    setInterval(function () {
        process(loadavgHistory600, memoryHistory600,
            networkHistory600, storageHistory600, temperatureHistory600,
            loadavgHistory7200, memoryHistory7200, networkHistory7200,
            storageHistory7200, temperatureHistory7200, 72, 20)
    }, 1000 * 60 * 60 * 24 * 5)

    setInterval(function () {
        process(loadavgHistory120, memoryHistory120,
            networkHistory120, storageHistory120, temperatureHistory120,
            loadavgHistory600, memoryHistory600, networkHistory600,
            storageHistory600, temperatureHistory600, 72, 5)
    }, 1000 * 60 * 60 * 10)

    setInterval(function () {
        process(loadavgHistory20, memoryHistory20,
            networkHistory20, storageHistory20, temperatureHistory20,
            loadavgHistory120, memoryHistory120, networkHistory120,
            storageHistory120, temperatureHistory120, 84, 6)
    }, 1000 * 60 * 60 * 2)

    setInterval(function () {
        process(loadavgHistory, memoryHistory,
            networkHistory, storageHistory, temperatureHistory,
            loadavgHistory20, memoryHistory20, networkHistory20,
            storageHistory20, temperatureHistory20, 144, 20)
    }, 1000 * 60 * 20)

    ;(function () {

        var prevSent, prevReceived, prevCheckTime
    
        setInterval(function () {

            function finish () {
                finished++
                if (finished == 2) {
                    var time = Date.now()
                    uptimeTotal += time - programLastTime
                    uptimeTotal = Math.floor(uptimeTotal)
                    programLastTime = time
                    onUpdate()
                }
            }

            function push (values, value) {
                pushLimit(values, value, 60)
            }

            var finished = 0

            var time = Date.now()
            push(loadavgHistory, {
                time: time,
                value: os.loadavg(),
            })
            push(memoryHistory, {
                time: time,
                value: os.freemem(),
            })

            ReadNetwork(function (sent, received) {
                if (prevCheckTime) {
                    var elapsedSeconds = (time - prevCheckTime) / 1000
                    push(networkHistory, {
                        time: time,
                        sent: (sent - prevSent) / elapsedSeconds,
                        received: (received - prevReceived) / elapsedSeconds,
                    })
                }
                prevSent = sent
                prevReceived = received
                prevCheckTime = time
                finish()
            })

            ReadStorage(function (newStorage) {
                storage = newStorage
                push(storageHistory, {
                    time: time,
                    total: storage.total,
                    used: newStorage.used,
                    fileSystems: newStorage.fileSystems,
                })
                finish()
            })

            ReadTemperature(function (temperature) {
                push(temperatureHistory, {
                    time: time,
                    cpu: temperature.cpu,
                    mb: temperature.mb,
                })
                finish()
            })

        }, 1000 * 60)

    })()

    return {
        getStatus: function () {

            ReadStorage(function (newStorage) {
                storage = newStorage
            })

            var totalMemory = os.totalmem()
    
            return {
                dateNow: Date.now(),
                uptime: os.uptime(),
                loadavg: os.loadavg(),
                numCpus: os.cpus().length,
                storage: storage,
                memory: {
                    total: totalMemory,
                    used: totalMemory - os.freemem(),
                },
                loadavgHistory: loadavgHistory,
                loadavgHistory20: loadavgHistory20,
                loadavgHistory120: loadavgHistory120,
                loadavgHistory600: loadavgHistory600,
                loadavgHistory7200: loadavgHistory7200,
                memoryHistory: memoryHistory,
                memoryHistory20: memoryHistory20,
                memoryHistory120: memoryHistory120,
                memoryHistory600: memoryHistory600,
                memoryHistory7200: memoryHistory7200,
                networkHistory: networkHistory,
                networkHistory20: networkHistory20,
                networkHistory120: networkHistory120,
                networkHistory600: networkHistory600,
                networkHistory7200: networkHistory7200,
                storageHistory: storageHistory,
                storageHistory20: storageHistory20,
                storageHistory120: storageHistory120,
                storageHistory600: storageHistory600,
                storageHistory7200: storageHistory7200,
                temperatureHistory: temperatureHistory,
                temperatureHistory20: temperatureHistory20,
                temperatureHistory120: temperatureHistory120,
                temperatureHistory600: temperatureHistory600,
                temperatureHistory7200: temperatureHistory7200,
                uptimeTotal: uptimeTotal,
                downtimeTotal: downtimeTotal,
            }

        },
        getState: function () {
            return {
                loadavgHistory: loadavgHistory,
                loadavgHistory20: loadavgHistory20,
                loadavgHistory120: loadavgHistory120,
                loadavgHistory600: loadavgHistory600,
                loadavgHistory7200: loadavgHistory7200,
                memoryHistory: memoryHistory,
                memoryHistory20: memoryHistory20,
                memoryHistory120: memoryHistory120,
                memoryHistory600: memoryHistory600,
                memoryHistory7200: memoryHistory7200,
                networkHistory: networkHistory,
                networkHistory20: networkHistory20,
                networkHistory120: networkHistory120,
                networkHistory600: networkHistory600,
                networkHistory7200: networkHistory7200,
                storageHistory: storageHistory,
                storageHistory20: storageHistory20,
                storageHistory120: storageHistory120,
                storageHistory600: storageHistory600,
                storageHistory7200: storageHistory7200,
                temperatureHistory: temperatureHistory,
                temperatureHistory20: temperatureHistory20,
                temperatureHistory120: temperatureHistory120,
                temperatureHistory600: temperatureHistory600,
                temperatureHistory7200: temperatureHistory7200,
                programLastTime: programLastTime,
                uptimeTotal: uptimeTotal,
                downtimeTotal: downtimeTotal,
            }
        },
        setState: function (state) {

            loadavgHistory = state.loadavgHistory
            loadavgHistory20 = state.loadavgHistory20 || []
            loadavgHistory120 = state.loadavgHistory120 || []
            loadavgHistory600 = state.loadavgHistory600 || []
            loadavgHistory7200 = state.loadavgHistory7200 || []
            memoryHistory = state.memoryHistory
            memoryHistory20 = state.memoryHistory20 || []
            memoryHistory120 = state.memoryHistory120 || []
            memoryHistory600 = state.memoryHistory600 || []
            memoryHistory7200 = state.memoryHistory7200 || []
            networkHistory = state.networkHistory
            networkHistory20 = state.networkHistory20 || []
            networkHistory120 = state.networkHistory120 || []
            networkHistory600 = state.networkHistory600 || []
            networkHistory7200 = state.networkHistory7200 || []
            storageHistory = state.storageHistory
            storageHistory20 = state.storageHistory20 || []
            storageHistory120 = state.storageHistory120 || []
            storageHistory600 = state.storageHistory600 || []
            storageHistory7200 = state.storageHistory7200 || []
            temperatureHistory = state.temperatureHistory || []
            temperatureHistory20 = state.temperatureHistory20 || []
            temperatureHistory120 = state.temperatureHistory120 || []
            temperatureHistory600 = state.temperatureHistory600 || []
            temperatureHistory7200 = state.temperatureHistory7200 || []
            programLastTime = state.programLastTime
            uptimeTotal = state.uptimeTotal
            downtimeTotal = state.downtimeTotal

            var osStartTime = Date.now() - os.uptime() * 1000
            if (osStartTime > programLastTime) {
                downtimeTotal += osStartTime - programLastTime
                downtimeTotal = Math.floor(downtimeTotal)
            }
            programLastTime = Date.now()

        },
    }

}

var os = require('os'),
    ReadNetwork = require('./ReadNetwork.js'),
    ReadStorage = require('./ReadStorage.js'),
    ReadTemperature = require('./ReadTemperature.js')

module.exports = Monitor
