function ReadNetwork (callback) {
    fs.readFile('/proc/net/dev', 'utf8', function (err, content) {
        var sent = 0, received = 0
        var lines = content.split(/\n/)
        lines.shift()
        lines.shift()
        lines.pop()
        lines.forEach(function (line) {
            var values = line.replace(/^\s+/, '').split(/:?\s+/)
            if (values[0] != 'lo') {
                sent += parseInt(values[9], 10)
                received += parseInt(values[1], 10)
            }
        })
        callback(sent, received)
    })
}

var fs = require('fs')
module.exports = ReadNetwork
