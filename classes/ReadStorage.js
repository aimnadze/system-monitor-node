function ReadStorage (callback) {
    var df = child_process.spawn('df', ['-t', 'ext4'])
    var stdout = ''
    df.stdout.on('data', function (data) {
        stdout += data
    })
    df.on('close', function () {
        var fileSystems = [],
            used = 0,
            total = 0
        var lines = stdout.split(/\n/)
        lines.shift()
        lines.pop()
        lines.forEach(function (line) {
            var values = line.replace(/^\s+/, '').split(/\s+/)
            var itemUsed = parseInt(values[2], 10)
            if (isFinite(itemUsed)) {
                var itemTotal = parseInt(values[3], 10) + itemUsed
                if (isFinite(itemTotal)) {
                    total += itemTotal
                    used += itemUsed
                    fileSystems.push({
                        total: itemTotal,
                        used: itemUsed,
                        mountPoint: values[5],
                    })
                }
            }
        })
        callback({
            total: total,
            used: used,
            fileSystems: fileSystems,
        })
    })
}

var child_process = require('child_process')
module.exports = ReadStorage
