function ReadTemperature (callback) {

    var output = ''

    var sensors = child_process.spawn('sensors')
    sensors.on('error', function (err) {
        if (err.code !== 'ENOENT') throw err
    })
    sensors.stdout.on('data', function (data) {
        output += data
    })
    sensors.stdout.on('end', function () {

        var cpu = null
        var mb = null

        var modules = output.split('\n\n')
        modules.forEach(function (module) {

            function forEachKeyValue (callback) {
                lines.forEach(function (line) {
                    var keyValue = line.split(':', 2)
                    var key = keyValue[0]
                    var value = keyValue[1].replace(/^\s+/, '')
                    callback(key, value)
                })
            }

            var lines = module.split('\n')
            var name = lines.shift()
            if (name === 'coretemp-isa-0000') {

                var cores = []
                forEachKeyValue(function (key, value) {

                    if (!key.match(/Core \d+/)) return

                    var match = value.match(/(^[+-]\d+\.\d+)[ °]C/)
                    if (!match) return

                    cores.push(parseFloat(match[1]))

                })

                if (cpu === null) {
                    cpu = 0
                    cores.forEach(function (temperature) {
                        cpu += temperature
                    })
                    cpu /= cores.length
                }

            } else if (name === 'atk0110-acpi-0') {
                forEachKeyValue(function (key, value) {

                    if (key === 'CPU Temperature') {
                        var match = value.match(/(^[+-]\d+\.\d+) C/)
                        if (match) cpu = parseFloat(match[1])
                    }

                    if (key === 'MB Temperature') {
                        var match = value.match(/(^[+-]\d+\.\d+) C/)
                        if (match) mb = parseFloat(match[1])
                    }

                })
            }
        })

        callback({
            cpu: cpu,
            mb: mb,
        })

    })

}

var child_process = require('child_process')
module.exports = ReadTemperature
