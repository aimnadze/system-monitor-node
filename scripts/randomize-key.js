#!/usr/bin/env node

function randomPassword () {
    var password = ''
    for (var i = 0; i < 32; i++) {
        password += charset[Math.floor(Math.random() * charset.length)]
    }
    return password
}

process.chdir(__dirname)

var charset = (function () {

    function addRange (a, z) {
        for (var i = a.charCodeAt(0); i <= z.charCodeAt(0); i++) {
            charset += String.fromCharCode(i)
        }
    }

    var charset = ''
    addRange('a', 'z')
    addRange('A', 'Z')
    addRange('0', '9')
    return charset

})()

var crypto = require('crypto'),
    fs = require('fs'),
    config = require('../config.js')

var newPassword = randomPassword(),
    newPasswordSha1 = crypto.createHash('sha1').update(newPassword).digest('hex')

var content =
    'exports.port = ' + config.port + '\n' +
    'exports.host = ' + JSON.stringify(config.host) + '\n' +
    'exports.keySha1 = ' + JSON.stringify(newPasswordSha1) + '\n'

fs.writeFileSync('../config.js', content)

console.log('New key is ' + newPassword)
