#!/usr/bin/env node

process.chdir(__dirname)

var fs = require('fs'),
    config = require('../config.js')

var content =
    'exports.port = ' + config.port + '\n' +
    'exports.host = ' + JSON.stringify(config.host) + '\n' +
    'exports.keySha1 = ' + JSON.stringify('da39a3ee5e6b4b0d3255bfef95601890afd80709') + '\n'

fs.writeFileSync('../config.js', content)

console.log('New key is an empty string')
