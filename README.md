System Monitor Node
===================

A program collecting server usage data and serving it over HTTP. Optionally, secured by a key.

Scripts
-------
* `./restart.sh` - Start/restart the server.
* `./stop.sh` - Stop the server.
* `./scripts/randomize-key.js` - Generate a random key. Restart the server to make it effective.
* `./scripts/disable-key.js` - Clear the key. Restart the server to make it effective.

See Also
--------

* [System Monitor Viewer](https://gitlab.com/aimnadze/system-monitor-viewer)
* [System Monitor SMSGlobal Notifier](https://gitlab.com/aimnadze/system-monitor-smsglobal-notifier)
